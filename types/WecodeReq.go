// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// WecodeReq represents a structure
type WecodeReq struct {
	AutoColor bool      `json:"auto_color"`
	IsHyaline bool      `json:"is_hyaline"`
	LineColor LineColor `json:"line_color" validate:"nonzero"`
	Width     int       `json:"width" validate:"min=280,max=1280,nonzero"`
}

// Reset set WecodeReq zero value
func (s *WecodeReq) Reset() {
	s.AutoColor = false
	s.IsHyaline = false
	s.LineColor.Reset()
	s.Width = 0
}

// Validate check if the data is legal
func (s WecodeReq) Validate() error {
	return validator.Validate(s)
}
