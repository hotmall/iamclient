// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// GetMallPeerUserContext is the context of method GetMallPeerUser
type GetMallPeerUserContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	CustomerID string `json:"customer_id,omitempty" validate:"min=18,max=19,regexp=^[0-9]{18\\,19}$"`
	MallID     string `json:"mall_id" validate:"nonzero"`
}

// NewGetMallPeerUserContext new an context instance
func NewGetMallPeerUserContext() *GetMallPeerUserContext {
	return &GetMallPeerUserContext{}
}

// Validate check if the data is legal
func (s GetMallPeerUserContext) Validate() error {
	return validator.Validate(s)
}
