// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// NewDyncwordRespBody represents a structure
type NewDyncwordRespBody struct {
	Dyncword string `json:"dyncword" validate:"nonzero"`
}

// Reset set NewDyncwordRespBody zero value
func (s *NewDyncwordRespBody) Reset() {
	s.Dyncword = ""
}

// Validate check if the data is legal
func (s NewDyncwordRespBody) Validate() error {
	return validator.Validate(s)
}
