// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// CheckUserMobileReqBody represents a structure
type CheckUserMobileReqBody struct {
	CountryCode string `json:"country_code" validate:"nonzero"`
	Mobile      string `json:"mobile" validate:"nonzero"`
}

// Reset set CheckUserMobileReqBody zero value
func (s *CheckUserMobileReqBody) Reset() {
	s.CountryCode = ""
	s.Mobile = ""
}

// Validate check if the data is legal
func (s CheckUserMobileReqBody) Validate() error {
	return validator.Validate(s)
}
