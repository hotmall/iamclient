// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// UpdateUserPasswordContext is the context of method UpdateUserPassword
type UpdateUserPasswordContext struct {
	XAuthToken string `json:"X-Auth-Token,omitempty"`
	ID         string `json:"id" validate:"nonzero"`
}

// NewUpdateUserPasswordContext new an context instance
func NewUpdateUserPasswordContext() *UpdateUserPasswordContext {
	return &UpdateUserPasswordContext{}
}

// Validate check if the data is legal
func (s UpdateUserPasswordContext) Validate() error {
	return validator.Validate(s)
}
