// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ResetUserPasswordReqBody represents a structure
type ResetUserPasswordReqBody struct {
	CountryCode string `json:"country_code" validate:"nonzero"`
	Dyncword    string `json:"dyncword" validate:"nonzero"`
	Mobile      string `json:"mobile" validate:"nonzero"`
	NewPassword string `json:"new_password" validate:"nonzero"`
}

// Reset set ResetUserPasswordReqBody zero value
func (s *ResetUserPasswordReqBody) Reset() {
	s.CountryCode = ""
	s.Dyncword = ""
	s.Mobile = ""
	s.NewPassword = ""
}

// Validate check if the data is legal
func (s ResetUserPasswordReqBody) Validate() error {
	return validator.Validate(s)
}
