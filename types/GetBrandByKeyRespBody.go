// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// GetBrandByKeyRespBody represents a structure
type GetBrandByKeyRespBody struct {
	Brand Brand `json:"brand" validate:"nonzero"`
}

// Reset set GetBrandByKeyRespBody zero value
func (s *GetBrandByKeyRespBody) Reset() {
	s.Brand.Reset()
}

// Validate check if the data is legal
func (s GetBrandByKeyRespBody) Validate() error {
	return validator.Validate(s)
}
