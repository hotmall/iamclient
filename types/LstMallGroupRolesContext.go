// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// LstMallGroupRolesContext is the context of method LstMallGroupRoles
type LstMallGroupRolesContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	GroupID    string `json:"group_id" validate:"nonzero"`
	MallID     string `json:"mall_id" validate:"min=18,max=19,regexp=^[0-9]{18\\,19}$,nonzero"`
}

// NewLstMallGroupRolesContext new an context instance
func NewLstMallGroupRolesContext() *LstMallGroupRolesContext {
	return &LstMallGroupRolesContext{}
}

// Validate check if the data is legal
func (s LstMallGroupRolesContext) Validate() error {
	return validator.Validate(s)
}
