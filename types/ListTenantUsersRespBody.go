// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ListTenantUsersRespBody represents a structure
type ListTenantUsersRespBody struct {
	Users []User `json:"users" validate:"nonzero"`
}

// Reset set ListTenantUsersRespBody zero value
func (s *ListTenantUsersRespBody) Reset() {
	s.Users = s.Users[:0]
}

// Validate check if the data is legal
func (s ListTenantUsersRespBody) Validate() error {
	return validator.Validate(s)
}
