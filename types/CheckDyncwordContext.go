// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// CheckDyncwordContext is the context of method CheckDyncword
type CheckDyncwordContext struct{}

// NewCheckDyncwordContext new an context instance
func NewCheckDyncwordContext() *CheckDyncwordContext {
	return &CheckDyncwordContext{}
}

// Validate check if the data is legal
func (s CheckDyncwordContext) Validate() error {
	return validator.Validate(s)
}
