// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// AssignRoleToGroupOnShopContext is the context of method AssignRoleToGroupOnShop
type AssignRoleToGroupOnShopContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	GroupID    string `json:"group_id" validate:"nonzero"`
	RoleID     string `json:"role_id" validate:"nonzero"`
	ShopID     string `json:"shop_id" validate:"min=18,max=19,regexp=^[0-9]{18\\,19}$,nonzero"`
}

// NewAssignRoleToGroupOnShopContext new an context instance
func NewAssignRoleToGroupOnShopContext() *AssignRoleToGroupOnShopContext {
	return &AssignRoleToGroupOnShopContext{}
}

// Validate check if the data is legal
func (s AssignRoleToGroupOnShopContext) Validate() error {
	return validator.Validate(s)
}
