// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ListTenantsContext is the context of method ListTenants
type ListTenantsContext struct {
	XAuthToken string `json:"X-Auth-Token,omitempty"`
	DomainID   string `json:"domain_id,omitempty"`
}

// NewListTenantsContext new an context instance
func NewListTenantsContext() *ListTenantsContext {
	return &ListTenantsContext{}
}

// Validate check if the data is legal
func (s ListTenantsContext) Validate() error {
	return validator.Validate(s)
}
