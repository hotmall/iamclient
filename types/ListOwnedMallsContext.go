// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ListOwnedMallsContext is the context of method ListOwnedMalls
type ListOwnedMallsContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	OwnerID    string `json:"owner_id" validate:"nonzero"`
}

// NewListOwnedMallsContext new an context instance
func NewListOwnedMallsContext() *ListOwnedMallsContext {
	return &ListOwnedMallsContext{}
}

// Validate check if the data is legal
func (s ListOwnedMallsContext) Validate() error {
	return validator.Validate(s)
}
