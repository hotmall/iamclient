// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ClaimMallReqBody represents a structure
type ClaimMallReqBody struct {
	ClaimCode    string `json:"claim_code" validate:"nonzero"`
	ServicePhone Phone  `json:"service_phone" validate:"nonzero"`
}

// Reset set ClaimMallReqBody zero value
func (s *ClaimMallReqBody) Reset() {
	s.ClaimCode = ""
	s.ServicePhone.Reset()
}

// Validate check if the data is legal
func (s ClaimMallReqBody) Validate() error {
	return validator.Validate(s)
}
