// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ShowAuthTokenRespBody represents a structure
type ShowAuthTokenRespBody struct {
	Toke Token `json:"toke" validate:"nonzero"`
}

// Reset set ShowAuthTokenRespBody zero value
func (s *ShowAuthTokenRespBody) Reset() {
	s.Toke.Reset()
}

// Validate check if the data is legal
func (s ShowAuthTokenRespBody) Validate() error {
	return validator.Validate(s)
}
