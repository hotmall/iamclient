// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ShowShopByProjectContext is the context of method ShowShopByProject
type ShowShopByProjectContext struct {
	XAuthToken string `json:"X-Auth-Token,omitempty"`
	ProjectID  string `json:"project_id" validate:"nonzero"`
}

// NewShowShopByProjectContext new an context instance
func NewShowShopByProjectContext() *ShowShopByProjectContext {
	return &ShowShopByProjectContext{}
}

// Validate check if the data is legal
func (s ShowShopByProjectContext) Validate() error {
	return validator.Validate(s)
}
