// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

type EnumAccessAuthType string

const (
	EnumAccessAuthTypeTenant   EnumAccessAuthType = "tenant"
	EnumAccessAuthTypeCustomer EnumAccessAuthType = "customer"
)

// Reset set EnumAccessAuthType zero value
func (s *EnumAccessAuthType) Reset() {
	*s = ""
}
