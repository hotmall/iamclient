// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// GetMallBrandContext is the context of method GetMallBrand
type GetMallBrandContext struct {
	XAuthToken string `json:"X-Auth-Token,omitempty"`
	BrandID    string `json:"brand_id,omitempty"`
}

// NewGetMallBrandContext new an context instance
func NewGetMallBrandContext() *GetMallBrandContext {
	return &GetMallBrandContext{}
}

// Validate check if the data is legal
func (s GetMallBrandContext) Validate() error {
	return validator.Validate(s)
}
