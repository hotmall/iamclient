// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// UpdateShopMallReqBody represents a structure
type UpdateShopMallReqBody struct {
	Mall MallJoined `json:"mall" validate:"nonzero"`
}

// Reset set UpdateShopMallReqBody zero value
func (s *UpdateShopMallReqBody) Reset() {
	s.Mall.Reset()
}

// Validate check if the data is legal
func (s UpdateShopMallReqBody) Validate() error {
	return validator.Validate(s)
}
