// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// PutShopWifiCodeRespBody represents a structure
type PutShopWifiCodeRespBody struct {
	Info Wificode `json:"info" validate:"nonzero"`
}

// Reset set PutShopWifiCodeRespBody zero value
func (s *PutShopWifiCodeRespBody) Reset() {
	s.Info.Reset()
}

// Validate check if the data is legal
func (s PutShopWifiCodeRespBody) Validate() error {
	return validator.Validate(s)
}
