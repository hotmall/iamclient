// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// ListMyMallsRespBody represents a structure
type ListMyMallsRespBody struct {
	Malls []Mall `json:"malls" validate:"nonzero"`
}

// Reset set ListMyMallsRespBody zero value
func (s *ListMyMallsRespBody) Reset() {
	s.Malls = s.Malls[:0]
}

// Validate check if the data is legal
func (s ListMyMallsRespBody) Validate() error {
	return validator.Validate(s)
}
