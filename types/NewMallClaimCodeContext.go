// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// NewMallClaimCodeContext is the context of method NewMallClaimCode
type NewMallClaimCodeContext struct {
	XAuthToken string `json:"X-Auth-Token" validate:"nonzero"`
	MallID     string `json:"mall_id" validate:"min=18,max=19,regexp=^[0-9]{18\\,19}$,nonzero"`
}

// NewNewMallClaimCodeContext new an context instance
func NewNewMallClaimCodeContext() *NewMallClaimCodeContext {
	return &NewMallClaimCodeContext{}
}

// Validate check if the data is legal
func (s NewMallClaimCodeContext) Validate() error {
	return validator.Validate(s)
}
