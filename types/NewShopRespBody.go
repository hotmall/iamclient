// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// NewShopRespBody represents a structure
type NewShopRespBody struct {
	Shop Shop `json:"shop" validate:"nonzero"`
}

// Reset set NewShopRespBody zero value
func (s *NewShopRespBody) Reset() {
	s.Shop.Reset()
}

// Validate check if the data is legal
func (s NewShopRespBody) Validate() error {
	return validator.Validate(s)
}
