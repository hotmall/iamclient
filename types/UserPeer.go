// Packaage types DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package types

import (
	"gopkg.in/validator.v2"
)

// UserPeer represents a structure
type UserPeer struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	UID  string `json:"uid,omitempty"`
}

// Reset set UserPeer zero value
func (s *UserPeer) Reset() {
	s.ID = ""
	s.Name = ""
	s.UID = ""
}

// Validate check if the data is legal
func (s UserPeer) Validate() error {
	return validator.Validate(s)
}
