// DO NOT EDIT THIS FILE. This file will be overwritten when re-running go-raml.

package iamclient

import (
	"encoding/json"

	"github.com/hotmall/requests"

	"gitlab.com/hotmall/iamclient/goraml"
	"gitlab.com/hotmall/iamclient/types"
)

// UserAPI represents an API structure
type UserAPI struct {
	baseURI string
}

// NewUserAPI new an API object
func NewUserAPI(host string) UserAPI {
	baseURI := host + "/iam/v1/users"
	return UserAPI{
		baseURI: baseURI,
	}
}

// ListUsers list tenant/domain all users
func (s UserAPI) ListUsers(reqContext *types.ListUsersContext) (respHeader map[string]string, respBody types.ListUsersRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}

	resp, err := requests.Request("GET", s.baseURI+"/", h)
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// NewUser New an user
func (s UserAPI) NewUser(reqContext *types.NewUserContext, reqBody types.NewUserReqBody) (respHeader map[string]string, respBody types.NewUserRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("POST", s.baseURI+"/", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 201 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// CheckUserMobile 校验用户手机是否全局唯一，在使能用户中
func (s UserAPI) CheckUserMobile(reqContext *types.CheckUserMobileContext, reqBody types.CheckUserMobileReqBody) (respHeader map[string]string, respBody types.CheckUserMobileRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("POST", s.baseURI+"/actions/checkmobile", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// CheckUserName 校验用户名在租户范围内是否唯一
func (s UserAPI) CheckUserName(reqContext *types.CheckUserNameContext, reqBody types.CheckUserNameReqBody) (respHeader map[string]string, respBody types.CheckUserNameRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("POST", s.baseURI+"/actions/checkname", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// ResetUserPassword 用户忘记密码重置密码
func (s UserAPI) ResetUserPassword(reqContext *types.ResetUserPasswordContext, reqBody types.ResetUserPasswordReqBody) (respHeader map[string]string, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("POST", s.baseURI+"/actions/resetpassword", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 204 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	return
}

// ShowUserByUid get user by tinode uid
func (s UserAPI) ShowUserByUid(reqContext *types.ShowUserByUidContext) (respHeader map[string]string, respBody types.ShowUserByUidRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	uID := reqContext.UID

	resp, err := requests.Request("GET", s.baseURI+"/uid/"+uID, h)
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// ShowUserByUserId get user by keystone user_id
func (s UserAPI) ShowUserByUserId(reqContext *types.ShowUserByUserIdContext) (respHeader map[string]string, respBody types.ShowUserByUserIdRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	userID := reqContext.UserID

	resp, err := requests.Request("GET", s.baseURI+"/user-id/"+userID, h)
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// UpdateUserMobile 用户修改绑定手机
func (s UserAPI) UpdateUserMobile(reqContext *types.UpdateUserMobileContext, reqBody types.UpdateUserMobileReqBody) (respHeader map[string]string, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	iD := reqContext.ID
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("PUT", s.baseURI+"/"+iD+"/mobile", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 204 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	return
}

// UpdateUserPassword 用户修改密码
func (s UserAPI) UpdateUserPassword(reqContext *types.UpdateUserPasswordContext, reqBody types.UpdateUserPasswordReqBody) (respHeader map[string]string, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	iD := reqContext.ID
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("PUT", s.baseURI+"/"+iD+"/password", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 204 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	return
}

// UpdateUser update a specific user
func (s UserAPI) UpdateUser(reqContext *types.UpdateUserContext, reqBody types.UserPure) (respHeader map[string]string, respBody types.UpdateUserRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	iD := reqContext.ID
	if err = reqBody.Validate(); err != nil {
		return
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return
	}

	resp, err := requests.Request("PUT", s.baseURI+"/"+iD+"/profile", h, requests.JSON(jsonBody))
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}

// ShowUser show a specific user
func (s UserAPI) ShowUser(reqContext *types.ShowUserContext) (respHeader map[string]string, respBody types.ShowUserRespBody, err error) {
	h := make(requests.Header)
	if reqContext.XAuthToken != "" {
		h["X-Auth-Token"] = reqContext.XAuthToken
	}
	iD := reqContext.ID

	resp, err := requests.Request("GET", s.baseURI+"/"+iD, h)
	if err != nil {
		return
	}

	respHeader = make(requests.Header, len(resp.Header))
	for hk, hv := range resp.Header {
		respHeader[hk] = hv
	}

	if resp.StatusCode != 200 {
		data := goraml.BasicError{}
		err = goraml.NewAPIError(resp, &data)
		e := err.(goraml.APIError)
		if e.Message != nil {
			err = data
		}
		return
	}
	if err = json.Unmarshal(resp.Content(), &respBody); err != nil {
		return
	}
	return
}
