package iamclient

//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/access_token.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/auth.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/auth_token.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/brand.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/customer.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/group.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/mall.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/mall_brand.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/project.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/role.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/shop.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/tenant.raml --import-path gitlab.com/hotmall/iamclient
//go:generate go-raml client --language go --kind requests --package iamclient --ramlfile api/user.raml --import-path gitlab.com/hotmall/iamclient

